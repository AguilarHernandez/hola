<?php 

namespace App\Entities;
use CodeIgniter\Entity;

class User extends Entity 
{
    public $id_usuario;
    public $username;
    public $email;
    public $password;
    public $id_competencia;
}