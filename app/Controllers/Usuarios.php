<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Entities\User;
use App\Models\Usuario;

class Usuarios extends Controller
{
	public function index()
	{
		$usuario = new Usuario();

		$data['usuarios'] = $usuario->getUsuarios();
		$data['competencia'] = $usuario->getCompetencia();
		echo view('templates/header', $data);
		echo view('usuario/overview');
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------

	public function delete($id)
	{
		$usuario = new Usuario();

		$msj = $usuario->eliminar($id);

		if ($msj == true) {
			echo "<script>alert('Eliminado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Eliminar!!!')</script>";
		}

		return redirect()->to('/crud_ci4_dao/public/Usuarios');
	}

	//--------------------------------------------------------------------

	public function insertar()
	{
		$usuario = new Usuario();

		// Create
		$user = new User();
		$data = [
			'username'=> $this->request->getPost('username'),
			'password'=> md5($this->request->getPost('password')),
			'email'=>$this->request->getPost('email'),
			'id_competencia'=>$this->request->getPost('competencia')
		];

		$user->fill($data);

		if ($usuario->save($user) == true) {
			echo "<script>alert('Agregado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Agregar!!!')</script>";
		}

		return redirect()->to('/crud_ci4_dao/public/Usuarios');
	}

	//--------------------------------------------------------------------

	public function getDatos($id)
	{
		$usuario = new Usuario();

		$data['usuarios'] = $usuario->getUsuario($id);
		$data['competencia'] = $usuario->getCompetencia();
		echo view('templates/header', $data);
		echo view('usuario/form');
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------

	public function actualizar()
	{
		$usuario = new Usuario();

		// Create
		$user = new User();
		$user->id_usuario 			= $this->request->getPost('id');
		$user->username 			= $this->request->getPost('username');
		$user->email    			= $this->request->getPost('email');
		$user->id_competencia    	= $this->request->getPost('competencia');

		$msj = $usuario->actualizar($user);

		if ($msj == true) {
			echo "<script>alert('Modificado Exitosamente!!!!!')</script>";
		}else{
			echo "<script>alert('Error al Modificar!!!')</script>";
		}

		return redirect()->to('/crud_ci4_dao/public/Usuarios');
	}

	//--------------------------------------------------------------------	
}
