<body>
<h2>Usuarios</h2>
<br>
<form action="<?php echo base_url('/crud_ci4_dao/public/Usuarios/insertar') ?>" method="post">
	<table>
		<tr>
			<td><label for="username">Usuario</label></td>
			<td><input type="text" name="username" id="username" class="form-control"></td>
		</tr>
		<tr>
			<td><label for="password">Contraseña</label></td>
			<td><input type="text" name="password" id="password" class="form-control"></td>
		</tr>
		<tr>
			<td><label for="email">Correo</label></td>
			<td><input type="text" name="email" id="email" class="form-control"></td>
		</tr>
		<tr>
			<td><label for="email">Competencia</label></td>
			<td>
				<select name="competencia" id="competencia" class="form-control">
					<option value="">--Seleccione su Competencia--</option>
					<?php foreach ($competencia as $comp) { ?>
						<option value="<?php echo $comp->id_competencia ?>"><?php echo $comp->nombre_competencia ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>						
	</table>
	<br>
	<input type="submit" name="Enviar" class="btn btn-primary">
	<br>
</form>
<br>
<table border="1">
	<thead>
		<tr>
			<th>Usuario</th>
			<th>Correo</th>
			<th>Competencia</th>
			<th colspan="2">ID</th>
		</tr>
	</thead>
	<tbody>		
		<?php foreach ($usuarios as $uc) { ?>
			<tr>
				<td><?php echo $uc->username; ?></td>
				<td><?php echo $uc->email; ?></td>
				<td><?php echo $uc->nombre_competencia; ?></td>
				<td><a class="btn btn-danger" onclick="return confirm('Desea realmente eliminar este dato?')" href="/crud_ci4_dao/public/Usuarios/delete/<?php echo $uc->id_usuario;  ?>">Eliminar</a></td>
				<td><a class="btn btn-info" href="/crud_ci4_dao/public/Usuarios/getDatos/<?php echo $uc->id_usuario;  ?>">Modificar</a></td>
			</tr>
		<?php } ?>

	</tbody>
</table>