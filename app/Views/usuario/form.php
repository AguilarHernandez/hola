<body>┬
<h2>Usuarios - Actualizar</h2>
<br>
<form action="<?php echo base_url('/crud_ci4_dao/public/Usuarios/actualizar') ?>" method="post">
	<table>
		<tr>
			<td><label for="password">ID</label></td>
			<td><input type="text" name="id" id="id" class="form-control" readonly="" value="<?php echo $usuarios->id_usuario; ?>"></td>
		</tr>
		<tr>
			<td><label for="username">Usuario</label></td>
			<td><input type="text" name="username" id="username" class="form-control" value="<?php echo $usuarios->username ?>"></td>
		</tr>
		<tr>
			<td><label for="email">Correo</label></td>
			<td><input type="text" name="email" id="email" class="form-control" value="<?php echo $usuarios->email ?>"></td>
		</tr>
		<tr>
			<td><label for="email">Competencia</label></td>
			<td>
				<select name="competencia" id="competencia" class="form-control">
					<option value="">--Seleccione su Competencia--</option>
					<?php foreach ($competencia as $comp) { ?>
						<option value="<?php echo $comp->id_competencia ?>" <?php if ($comp->id_competencia == $usuarios->id_competencia) { echo "selected"; } ?>><?php echo $comp->nombre_competencia ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>						
	</table>
	<br>
	<input type="submit" name="Enviar" class="btn btn-primary">
	<br>
</form>

