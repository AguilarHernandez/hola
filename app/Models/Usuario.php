<?php 

namespace App\Models;
use CodeIgniter\Model;
use App\Entities\User;

class Usuario extends Model
{
    protected $table      = 'usuario';
    protected $primaryKey = 'id_usuario';

    protected $returnType = 'App\Entities\User';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['id_usuario', 'username', 'email', 'password', 'id_competencia'];

    protected $useTimestamps = false;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = true;

	//--------------------------------------------------------------------

	public function getUsuarios()
	{
		$sp_consultar_uc = 'CALL sp_consultar_uc()';
		$query = $this->query($sp_consultar_uc);

		return $query->getResult();
	}

	//--------------------------------------------------------------------

	public function eliminar($id)
	{
		$sp_eliminar_usuario = 'CALL sp_eliminar_usuario(?)';
		$query = $this->query($sp_eliminar_usuario, [$id]);

		if ($query) {
			return true;
		}else{
			return false;
		}
	}

	//--------------------------------------------------------------------

	public function getCompetencia()
	{
		$sp_consultar_competencias = 'CALL sp_consultar_competencias()';
		$query = $this->query($sp_consultar_competencias);

		return $query->getResult();
	}

	//--------------------------------------------------------------------

	public function getUsuario($id)
	{
		$sp_consultarporid = 'CALL sp_consultarporid(?)';
		$query = $this->query($sp_consultarporid, [$id]);

		if ($query) {
			foreach ($query->getResult() as $row)
				{
				    // Create
					$user = new User();
					$user->id_usuario 			= $row->id_usuario;
					$user->username 			= $row->username;
					$user->email    			= $row->email;;
					$user->password    			= $row->password;
					$user->id_competencia    	= $row->id_competencia;

					return $user;
				}
		}else{
			return false;
		}
	}

	//--------------------------------------------------------------------

	public function actualizar($user)
	{
		$sp_modificar_usuario = 'CALL sp_modificar_usuario(?, ?, ?, ?)';
		$query = $this->query($sp_modificar_usuario, [$user->id_usuario ,$user->username,$user->email ,$user->id_competencia]);

		if ($query) {
			return "modi";
		}else{
			return "errorM";
		}
	}	

}